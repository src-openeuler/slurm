Name:		slurm
Version:	21.08.8
%define rel	57
Release:	%{rel}%{?dist}
Summary:	Slurm Workload Manager

Group:		System Environment/Base
License:	GPLv2+
URL:		https://slurm.schedmd.com/

# when the rel number is one, the directory name does not include it
%if "%{rel}" == "1"
%global slurm_source_dir %{name}-%{version}
%else
%global slurm_source_dir %{name}-%{version}-2
%endif

Source:		%{slurm_source_dir}.tar.bz2
Patch1: 0001-support-oversubscribe-in-submit_job-restapi.patch
Patch2: 0002-add-pgsql-plugin.patch
Patch3: 0003-replace-with-AND-in-as_pgsql_resource.patch
Patch4: 0004-fix-some-sql-bug.patch 
Patch5: 0005-replace-in-as_pgsql_jobacct_process.patch
Patch6: 0006-add-return-value-SQL_NO_DATA-in-sql-api.patch
Patch7: 0007-replace-and-in-sql-in-some-file.patch
Patch8: 0008-change-sql-datatype.patch
Patch9: 0009-fix-the-default-value-type.patch
Patch10: 0010-use-pgsql_handle_quotes-to-replace-old-method.patch
Patch11: 0011-Adjusting-the-implementation-of-last_affect_row.patch
Patch12: 0012-fix-some-sql-statements.patch
Patch13: 0013-fixed-the-partition-field-in-sql.patch
Patch14: 0014-remove-FOR-UPDATE-in-sql.patch
Patch15: 0015-fix-partition-conflict-in-tables.patch
Patch16: 0016-resume-autocommit-off-option.patch
Patch17: 0017-update-jobcomp_table-field.patch
Patch18: 0018-update-get_parent_limits-stored-procedure.patch
Patch19: 0019-Adjusting-some-SQL-statements.patch
Patch20: 0020-replace-UNIX_TIMESTAMP-in-sql.patch
Patch21: 0021-fix-user-and-partition-field-in-sql.patch
Patch22: 0022-add-SQLRowCount-exception-handling.patch
Patch23: 0023-modify-step-table.patch
Patch24: 0024-update-the-implementation-of-obtaining-the-last-resu.patch
Patch25: 0025-Adjusting-the-commit-off-setting-position.patch
Patch26: 0026-Remove-excess-double-quotes-in-assoc-table.patch
Patch27: 0027-Adjusting-procedures.patch
Patch28: 0028-modify-the-field-type-of-info-in-txn_table.patch
Patch29: 0029-add-partition-default-value-in-job_table.patch
Patch30: 0030-Modify-the-alias-after-EXCLUDED-in-SQL-to-field-name.patch
Patch31: 0031-fix-some-error-add-pgsql_escape_str-function.patch
Patch32: 0032-Correcting-archive-SQL-statements-in-tables.patch
Patch33: 0033-fix-pgsql_db_insert_ret_id.patch
Patch34: 0034-Replace-the-REPLACE-function-with-the-REGEXP_REPLACE.patch
Patch35: 0035-fix-the-sql-error-in-adding-resource.patch
Patch36: 0036-fix-the-DefaultWCKey-not-working.patch
Patch37: 0037-fix-adding-tres-resource-failure.patch
Patch38: 0038-fix-querying-the-user-binding-qos.patch
Patch39: 0039-fix-association-GrpTRES-get_parent_limits-errors.patch
Patch40: 0040-Remove-redundant-SQL-statements-in-adding-tres.patch
Patch41: 0041-Fix-compilation-warnings.patch
Patch42: 0042-Change-the-serial-type-to-integer.patch
Patch43: 0043-Remove-redundant-tres_table-fields.patch
Patch44: 0044-replace-goto-fini.patch
Patch45: 0045-Remove-comments-in-concatenated-strings.patch
Patch46: 0046-Add-empty-check-and-inline-in-set_autocommit_off.patch
Patch47: 0047-rename-_get_result_by_index.patch
Patch48: 0048-fix-sacct-not-showing-job-information.patch
Patch49: 0049-fix-the-preempt-error.patch
Patch50: 0050-enable-col_data-empty-check.patch
Patch51: 0051-fix-the-archive-error.patch
Patch52: 0052-fix-sacctmgr-delete-error-with-flags-specified.patch
Patch53: 0053-add-slurmrestd-log.patch
Patch54: 0054-add-restapi-for-scontrol-reconfigure.patch

# build options		.rpmmacros options	change to default action
# ====================  ====================	========================
# --prefix		%_prefix path		install path for commands, libraries, etc.
# --with cray		%_with_cray 1		build for a Cray Aries system
# --with cray_network	%_with_cray_network 1	build for a non-Cray system with a Cray network
# --with cray_shasta	%_with_cray_shasta 1	build for a Cray Shasta system
# --with slurmrestd	%_with_slurmrestd 1	build slurmrestd
# --with slurmsmwd      %_with_slurmsmwd 1      build slurmsmwd
# --without debug	%_without_debug 1	don't compile with debugging symbols
# --with hdf5		%_with_hdf5 path	require hdf5 support
# --with hwloc		%_with_hwloc 1		require hwloc support
# --with lua		%_with_lua path		build Slurm lua bindings
# --with mysql		%_with_mysql 1		require mysql/mariadb support
# --with pgsql		%_with_pgsql 1		require unixODBC support
# --with numa		%_with_numa 1		require NUMA support
# --without pam		%_without_pam 1		don't require pam-devel RPM to be installed
# --without x11		%_without_x11 1		disable internal X11 support
# --with ucx		%_with_ucx path		require ucx support
# --with pmix		%_with_pmix path	require pmix support
# --with nvml		%_with_nvml path	require nvml support
#

#  Options that are off by default (enable with --with <opt>)
%bcond_with cray
%bcond_with cray_network
%bcond_with cray_shasta
%bcond_with slurmrestd
%bcond_with slurmsmwd
%bcond_with multiple_slurmd
%bcond_with ucx

# These options are only here to force there to be these on the build.
# If they are not set they will still be compiled if the packages exist.
%bcond_with hwloc
%bcond_with mysql
%bcond_with pgsql
%bcond_with hdf5
%bcond_with lua
%bcond_with numa
%bcond_with pmix
%bcond_with nvml

# Use debug by default on all systems
%bcond_without debug

# Options enabled by default
%bcond_without pam
%bcond_without x11

# Disable hardened builds. -z,now or -z,relro breaks the plugin stack
%undefine _hardened_build
%global _hardened_cflags "-Wl,-z,lazy"
%global _hardened_ldflags "-Wl,-z,lazy"

Requires: munge  

%{?systemd_requires}
BuildRequires: systemd
BuildRequires: munge-devel munge-libs
BuildRequires: python3
BuildRequires: readline-devel
BuildRequires: perl-devel
BuildRequires: mariadb-devel
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: libtool
Obsoletes: slurm-lua slurm-munge slurm-plugins

# fake systemd support when building rpms on other platforms
%{!?_unitdir: %global _unitdir /lib/systemd/systemd}

%define use_mysql_devel %(perl -e '`rpm -q mariadb-devel`; print $?;')

%if %{with mysql}
%if %{use_mysql_devel}
BuildRequires: mysql-devel >= 5.0.0
%else
BuildRequires: mariadb-devel >= 5.0.0
%endif
%endif

%if %{with pgsql}
BuildRequires: unixODBC-devel >= 2.3.7
%endif

%if %{with cray}
BuildRequires: cray-libalpscomm_cn-devel
BuildRequires: cray-libalpscomm_sn-devel
BuildRequires: libnuma-devel
BuildRequires: libhwloc-devel
BuildRequires: cray-libjob-devel
BuildRequires: gtk2-devel
BuildRequires: glib2-devel
BuildRequires: pkg-config
%endif

%if %{with cray_network}
%if %{use_mysql_devel}
BuildRequires: mysql-devel
%else
BuildRequires: mariadb-devel
%endif
BuildRequires: cray-libalpscomm_cn-devel
BuildRequires: cray-libalpscomm_sn-devel
BuildRequires: hwloc-devel
BuildRequires: gtk2-devel
BuildRequires: glib2-devel
BuildRequires: pkgconfig
%endif

BuildRequires: perl(ExtUtils::MakeMaker)

%if %{with lua}
BuildRequires: pkgconfig(lua) >= 5.1.0
%endif

%if %{with hwloc}
BuildRequires: hwloc-devel
%endif

%if %{with numa}
%if %{defined suse_version}
BuildRequires: libnuma-devel
%else
BuildRequires: numactl-devel
%endif
%endif

%if %{with pmix} && "%{_with_pmix}" == "--with-pmix"
BuildRequires: pmix
%global pmix_version %(rpm -q pmix --qf "%{RPMTAG_VERSION}")
%endif

%if %{with ucx} && "%{_with_ucx}" == "--with-ucx"
BuildRequires: ucx-devel
%global ucx_version %(rpm -q ucx-devel --qf "%{RPMTAG_VERSION}")
%endif

#  Allow override of sysconfdir via _slurm_sysconfdir.
#  Note 'global' instead of 'define' needed here to work around apparent
#   bug in rpm macro scoping (or something...)
%{!?_slurm_sysconfdir: %global _slurm_sysconfdir /etc/slurm}
%define _sysconfdir %_slurm_sysconfdir

#  Allow override of datadir via _slurm_datadir.
%{!?_slurm_datadir: %global _slurm_datadir %{_prefix}/share}
%define _datadir %{_slurm_datadir}

#  Allow override of mandir via _slurm_mandir.
%{!?_slurm_mandir: %global _slurm_mandir %{_datadir}/man}
%define _mandir %{_slurm_mandir}

#
# Never allow rpm to strip binaries as this will break
#  parallel debugging capability
# Note that brp-compress does not compress man pages installed
#  into non-standard locations (e.g. /usr/local)
#
%define __os_install_post /usr/lib/rpm/brp-compress
%define debug_package %{nil}

#
# Should unpackaged files in a build root terminate a build?
# Uncomment if needed again.
#%define _unpackaged_files_terminate_build      0

# Slurm may intentionally include empty manifest files, which will
# cause errors with rpm 4.13 and on. Turn that check off.
%define _empty_manifest_terminate_build 0

# First we remove $prefix/local and then just prefix to make
# sure we get the correct installdir
%define _perlarch %(perl -e 'use Config; $T=$Config{installsitearch}; $P=$Config{installprefix}; $P1="$P/local"; $T =~ s/$P1//; $T =~ s/$P//; print $T;')

%define _perlman3 %(perl -e 'use Config; $T=$Config{installsiteman3dir}; $P=$Config{siteprefix}; $P1="$P/local"; $T =~ s/$P1//; $T =~ s/$P//; print $T;')

%define _perlarchlib %(perl -e 'use Config; $T=$Config{installarchlib}; $P=$Config{installprefix}; $P1="$P/local"; $T =~ s/$P1//; $T =~ s/$P//; print $T;')

%define _perldir %{_prefix}%{_perlarch}
%define _perlman3dir %{_prefix}%{_perlman3}
%define _perlarchlibdir %{_prefix}%{_perlarchlib}

%description
Slurm is an open source, fault-tolerant, and highly scalable
cluster management and job scheduling system for Linux clusters.
Components include machine status, partition management,
job management, scheduling and accounting modules

%package perlapi
Summary: Perl API to Slurm
Group: Development/System
Requires: %{name}%{?_isa} = %{version}-%{release}
%description perlapi
Perl API package for Slurm.  This package includes the perl API to provide a
helpful interface to Slurm through Perl

%package devel
Summary: Development package for Slurm
Group: Development/System
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
Development package for Slurm.  This package includes the header files
and static libraries for the Slurm API

%package example-configs
Summary: Example config files for Slurm
Group: Development/System
%description example-configs
Example configuration files for Slurm.

%package slurmctld
Summary: Slurm controller daemon
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
%description slurmctld
Slurm controller daemon. Used to manage the job queue, schedule jobs,
and dispatch RPC messages to the slurmd processon the compute nodes
to launch jobs.

%package slurmd
Summary: Slurm compute node daemon
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
%if %{with pmix} && "%{_with_pmix}" == "--with-pmix"
Requires: pmix = %{pmix_version}
%endif
%if %{with ucx} && "%{_with_ucx}" == "--with-ucx"
Requires: ucx = %{ucx_version}
%endif
%description slurmd
Slurm compute node daemon. Used to launch jobs on compute nodes

%package slurmdbd
Summary: Slurm database daemon
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
Obsoletes: slurm-sql
%description slurmdbd
Slurm database daemon. Used to accept and process database RPCs and upload
database changes to slurmctld daemons on each cluster

%package libpmi
Summary: Slurm\'s implementation of the pmi libraries
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
Conflicts: pmix-libpmi
%description libpmi
Slurm\'s version of libpmi. For systems using Slurm, this version
is preferred over the compatibility libraries shipped by the PMIx project.

%package torque
Summary: Torque/PBS wrappers for transition from Torque/PBS to Slurm
Group: Development/System
Requires: slurm-perlapi
%description torque
Torque wrapper scripts used for helping migrate from Torque/PBS to Slurm

%package openlava
Summary: openlava/LSF wrappers for transition from OpenLava/LSF to Slurm
Group: Development/System
Requires: slurm-perlapi
%description openlava
OpenLava wrapper scripts used for helping migrate from OpenLava/LSF to Slurm

%package contribs
Summary: Perl tool to print Slurm job state information
Group: Development/System
Requires: %{name}%{?_isa} = %{version}-%{release}
Obsoletes: slurm-sjobexit slurm-sjstat slurm-seff
%description contribs
seff is a mail program used directly by the Slurm daemons. On completion of a
job, wait for it's accounting information to be available and include that
information in the email body.
sjobexit is a slurm job exit code management tool. It enables users to alter
job exit code information for completed jobs
sjstat is a Perl tool to print Slurm job state information. The output is designed
to give information on the resource usage and availablilty, as well as information
about jobs that are currently active on the machine. This output is built
using the Slurm utilities, sinfo, squeue and scontrol, the man pages for these
utilities will provide more information and greater depth of understanding.

%if %{with pam}
%package pam_slurm
Summary: PAM module for restricting access to compute nodes via Slurm
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: pam-devel
Obsoletes: pam_slurm
%description pam_slurm
This module restricts access to compute nodes in a cluster where Slurm is in
use.  Access is granted to root, any user with an Slurm-launched job currently
running on the node, or any user who has allocated resources on the node
according to the Slurm
%endif

%if %{with slurmrestd}
%package slurmrestd
Summary: Slurm REST API translator
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
BuildRequires: http-parser-devel
%if %{defined suse_version}
BuildRequires: libjson-c-devel
%else
BuildRequires: json-c-devel
%endif
%description slurmrestd
Provides a REST interface to Slurm.
%endif

%if %{with slurmsmwd}
%package slurmsmwd
Summary: support daemons and software for the Cray SMW
Group: System Environment/Base
Requires: %{name}%{?_isa} = %{version}-%{release}
Obsoletes: craysmw
%description slurmsmwd
support daemons and software for the Cray SMW.  Includes slurmsmwd which
notifies slurm about failed nodes.
%endif

#############################################################################

%prep
# when the rel number is one, the tarball filename does not include it
%autosetup -n %{slurm_source_dir} -p1

%build
autoreconf -ivf
%configure \
	%{?_without_debug:--disable-debug} \
	%{?_with_pam_dir} \
	%{?_with_cpusetdir} \
	%{?_with_mysql_config} \
	%{?_with_pgsql_config} \
	%{?_with_ssl} \
	%{?_without_cray:--enable-really-no-cray}\
	%{?_with_cray_network:--enable-cray-network}\
	%{?_with_multiple_slurmd:--enable-multiple-slurmd} \
	%{?_with_pmix} \
	%{?_with_freeipmi} \
	%{?_with_hdf5} \
	%{?_with_shared_libslurm} \
	%{!?_with_slurmrestd:--disable-slurmrestd} \
	%{?_without_x11:--disable-x11} \
	%{?_with_ucx} \
	%{?_with_nvml} \
	%{?_with_cflags}

make %{?_smp_mflags}

%install

# Ignore redundant standard rpaths and insecure relative rpaths,
# for RHEL based distros which use "check-rpaths" tool.
export QA_RPATHS=0x5

# Strip out some dependencies

cat > find-requires.sh <<'EOF'
exec %{__find_requires} "$@" | egrep -v '^libpmix.so|libevent|libnvidia-ml'
EOF
chmod +x find-requires.sh
%global _use_internal_dependency_generator 0
%global __find_requires %{_builddir}/%{buildsubdir}/find-requires.sh

rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
make install-contrib DESTDIR=%{buildroot}

install -D -m644 etc/slurmctld.service %{buildroot}/%{_unitdir}/slurmctld.service
install -D -m644 etc/slurmd.service    %{buildroot}/%{_unitdir}/slurmd.service
install -D -m644 etc/slurmdbd.service  %{buildroot}/%{_unitdir}/slurmdbd.service

%if %{with slurmrestd}
install -D -m644 etc/slurmrestd.service  %{buildroot}/%{_unitdir}/slurmrestd.service
%endif

# Do not package Slurm's version of libpmi on Cray systems in the usual location.
# Cray's version of libpmi should be used. Move it elsewhere if the site still
# wants to use it with other MPI stacks.
%if %{with cray} || %{with cray_shasta}
   mkdir %{buildroot}/%{_libdir}/slurmpmi
   mv %{buildroot}/%{_libdir}/libpmi* %{buildroot}/%{_libdir}/slurmpmi
%endif

%if %{with cray}
   install -D -m644 contribs/cray/plugstack.conf.template %{buildroot}/%{_sysconfdir}/plugstack.conf.template
   install -D -m644 contribs/cray/slurm.conf.template %{buildroot}/%{_sysconfdir}/slurm.conf.template
   mkdir -p %{buildroot}/opt/modulefiles/slurm
   test -f contribs/cray/opt_modulefiles_slurm &&
      install -D -m644 contribs/cray/opt_modulefiles_slurm %{buildroot}/opt/modulefiles/slurm/%{version}-%{rel}
   echo -e '#%Module\nset ModulesVersion "%{version}-%{rel}"' > %{buildroot}/opt/modulefiles/slurm/.version
%else
   rm -f contribs/cray/opt_modulefiles_slurm
   rm -f %{buildroot}/%{_sysconfdir}/plugstack.conf.template
   rm -f %{buildroot}/%{_sysconfdir}/slurm.conf.template
   rm -f %{buildroot}/%{_sbindir}/capmc_suspend
   rm -f %{buildroot}/%{_sbindir}/capmc_resume
   rm -f %{buildroot}/%{_sbindir}/slurmconfgen.py
%endif

%if %{with slurmsmwd}
   install -D -m644 contribs/cray/slurmsmwd/slurmsmwd.service %{buildroot}/%{_unitdir}/slurmsmwd.service
%else
   rm -f %{buildroot}/%{_sbindir}/slurmsmwd
   rm -f contribs/cray/slurmsmwd/slurmsmwd.service
%endif

install -D -m644 etc/cgroup.conf.example %{buildroot}/%{_sysconfdir}/cgroup.conf.example
install -D -m644 etc/prolog.example %{buildroot}/%{_sysconfdir}/prolog.example
install -D -m644 etc/job_submit.lua.example %{buildroot}/%{_sysconfdir}/job_submit.lua.example
install -D -m644 etc/slurm.conf.example %{buildroot}/%{_sysconfdir}/slurm.conf.example
install -D -m600 etc/slurmdbd.conf.example %{buildroot}/%{_sysconfdir}/slurmdbd.conf.example
install -D -m644 etc/cli_filter.lua.example %{buildroot}/%{_sysconfdir}/cli_filter.lua.example
install -D -m755 contribs/sjstat %{buildroot}/%{_bindir}/sjstat

# Delete unpackaged files:
find %{buildroot} -name '*.a' -exec rm {} \;
find %{buildroot} -name '*.la' -exec rm {} \;
rm -f %{buildroot}/%{_libdir}/slurm/job_submit_defaults.so
rm -f %{buildroot}/%{_libdir}/slurm/job_submit_logging.so
rm -f %{buildroot}/%{_libdir}/slurm/job_submit_partition.so
rm -f %{buildroot}/%{_libdir}/slurm/auth_none.so
rm -f %{buildroot}/%{_libdir}/slurm/cred_none.so
rm -f %{buildroot}/%{_sbindir}/sfree
rm -f %{buildroot}/%{_sbindir}/slurm_epilog
rm -f %{buildroot}/%{_sbindir}/slurm_prolog
rm -f %{buildroot}/%{_sysconfdir}/init.d/slurm
rm -f %{buildroot}/%{_sysconfdir}/init.d/slurmdbd
rm -f %{buildroot}/%{_perldir}/auto/Slurm/.packlist
rm -f %{buildroot}/%{_perldir}/auto/Slurm/Slurm.bs
rm -f %{buildroot}/%{_perlarchlibdir}/perllocal.pod
rm -f %{buildroot}/%{_perldir}/perllocal.pod
rm -f %{buildroot}/%{_perldir}/auto/Slurmdb/.packlist
rm -f %{buildroot}/%{_perldir}/auto/Slurmdb/Slurmdb.bs

# Build man pages that are generated directly by the tools
rm -f %{buildroot}/%{_mandir}/man1/sjobexitmod.1
%{buildroot}/%{_bindir}/sjobexitmod --roff > %{buildroot}/%{_mandir}/man1/sjobexitmod.1
rm -f %{buildroot}/%{_mandir}/man1/sjstat.1
%{buildroot}/%{_bindir}/sjstat --roff > %{buildroot}/%{_mandir}/man1/sjstat.1

# Build conditional file list for main package
LIST=./slurm.files
touch $LIST
test -f %{buildroot}/%{_libexecdir}/slurm/cr_checkpoint.sh   &&
  echo %{_libexecdir}/slurm/cr_checkpoint.sh	        >> $LIST
test -f %{buildroot}/%{_libexecdir}/slurm/cr_restart.sh      &&
  echo %{_libexecdir}/slurm/cr_restart.sh	        >> $LIST
test -f %{buildroot}/%{_sbindir}/capmc_suspend		&&
  echo %{_sbindir}/capmc_suspend			>> $LIST
test -f %{buildroot}/%{_sbindir}/capmc_resume		&&
  echo %{_sbindir}/capmc_resume				>> $LIST
test -f %{buildroot}/%{_bindir}/netloc_to_topology		&&
  echo %{_bindir}/netloc_to_topology			>> $LIST

test -f %{buildroot}/opt/modulefiles/slurm/%{version}-%{rel} &&
  echo /opt/modulefiles/slurm/%{version}-%{rel} >> $LIST
test -f %{buildroot}/opt/modulefiles/slurm/.version &&
  echo /opt/modulefiles/slurm/.version >> $LIST


LIST=./example.configs
touch $LIST
%if %{with cray}
   test -f %{buildroot}/%{_sbindir}/slurmconfgen.py	&&
	echo %{_sbindir}/slurmconfgen.py		>>$LIST
%endif

# Make pkg-config file
mkdir -p %{buildroot}/%{_libdir}/pkgconfig
cat >%{buildroot}/%{_libdir}/pkgconfig/slurm.pc <<EOF
includedir=%{_prefix}/include
libdir=%{_libdir}

Cflags: -I\${includedir}
Libs: -L\${libdir} -lslurm
Description: Slurm API
Name: %{name}
Version: %{version}
EOF

LIST=./pam.files
touch $LIST
%if %{?with_pam_dir}0
    test -f %{buildroot}/%{with_pam_dir}/pam_slurm.so	&&
	echo %{with_pam_dir}/pam_slurm.so	>>$LIST
    test -f %{buildroot}/%{with_pam_dir}/pam_slurm_adopt.so	&&
	echo %{with_pam_dir}/pam_slurm_adopt.so	>>$LIST
%else
    test -f %{buildroot}/lib/security/pam_slurm.so	&&
	echo /lib/security/pam_slurm.so		>>$LIST
    test -f %{buildroot}/lib32/security/pam_slurm.so	&&
	echo /lib32/security/pam_slurm.so	>>$LIST
    test -f %{buildroot}/lib64/security/pam_slurm.so	&&
	echo /lib64/security/pam_slurm.so	>>$LIST
    test -f %{buildroot}/lib/security/pam_slurm_adopt.so		&&
	echo /lib/security/pam_slurm_adopt.so		>>$LIST
    test -f %{buildroot}/lib32/security/pam_slurm_adopt.so		&&
	echo /lib32/security/pam_slurm_adopt.so		>>$LIST
    test -f %{buildroot}/lib64/security/pam_slurm_adopt.so		&&
	echo /lib64/security/pam_slurm_adopt.so		>>$LIST
%endif
#############################################################################

%clean
rm -rf %{buildroot}
#############################################################################

%files -f slurm.files
%defattr(-,root,root,0755)
%{_datadir}/doc
%{_bindir}/s*
%exclude %{_bindir}/seff
%exclude %{_bindir}/sjobexitmod
%exclude %{_bindir}/sjstat
%exclude %{_bindir}/smail
%exclude %{_libdir}/libpmi*
%{_libdir}/*.so*
%{_libdir}/slurm/src/*
%{_libdir}/slurm/*.so
%exclude %{_libdir}/slurm/accounting_storage_mysql.so
%exclude %{_libdir}/slurm/accounting_storage_pgsql.so
%exclude %{_libdir}/slurm/job_submit_pbs.so
%exclude %{_libdir}/slurm/spank_pbs.so
%{_mandir}
%exclude %{_mandir}/man1/sjobexit*
%exclude %{_mandir}/man1/sjstat*
%dir %{_libdir}/slurm/src
%if %{with cray}
%dir /opt/modulefiles/slurm
%endif
#############################################################################

%files -f example.configs example-configs
%defattr(-,root,root,0755)
%dir %{_sysconfdir}
%if %{with cray}
%config %{_sysconfdir}/plugstack.conf.template
%config %{_sysconfdir}/slurm.conf.template
%endif
%config %{_sysconfdir}/cgroup.conf.example
%config %{_sysconfdir}/job_submit.lua.example
%config %{_sysconfdir}/prolog.example
%config %{_sysconfdir}/slurm.conf.example
%config %{_sysconfdir}/slurmdbd.conf.example
%config %{_sysconfdir}/cli_filter.lua.example
#############################################################################

%files devel
%defattr(-,root,root)
%dir %attr(0755,root,root)
%dir %{_prefix}/include/slurm
%{_prefix}/include/slurm/*
%dir %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/slurm.pc
#############################################################################

%files perlapi
%defattr(-,root,root)
%{_perldir}/Slurm.pm
%{_perldir}/Slurm/Bitstr.pm
%{_perldir}/Slurm/Constant.pm
%{_perldir}/Slurm/Hostlist.pm
%{_perldir}/auto/Slurm/Slurm.so
%{_perldir}/Slurmdb.pm
%{_perldir}/auto/Slurmdb/Slurmdb.so
%{_perldir}/auto/Slurmdb/autosplit.ix
%{_perlman3dir}/Slurm*

#############################################################################

%files slurmctld
%defattr(-,root,root)
%{_sbindir}/slurmctld
%{_unitdir}/slurmctld.service
#############################################################################

%files slurmd
%defattr(-,root,root)
%{_sbindir}/slurmd
%{_sbindir}/slurmstepd
%{_unitdir}/slurmd.service
#############################################################################

%files slurmdbd
%defattr(-,root,root)
%{_sbindir}/slurmdbd
%{_libdir}/slurm/accounting_storage_mysql.so
%if %{with pgsql}
%{_libdir}/slurm/accounting_storage_pgsql.so
%endif
%{_unitdir}/slurmdbd.service
#############################################################################

%files libpmi
%defattr(-,root,root)
%if %{with cray} || %{with cray_shasta}
%{_libdir}/slurmpmi/*
%else
%{_libdir}/libpmi*
%endif
#############################################################################

%files torque
%defattr(-,root,root)
%{_bindir}/pbsnodes
%{_bindir}/qalter
%{_bindir}/qdel
%{_bindir}/qhold
%{_bindir}/qrerun
%{_bindir}/qrls
%{_bindir}/qstat
%{_bindir}/qsub
%{_bindir}/mpiexec
%{_bindir}/generate_pbs_nodefile
%{_libdir}/slurm/job_submit_pbs.so
%{_libdir}/slurm/spank_pbs.so
#############################################################################

%files openlava
%defattr(-,root,root)
%{_bindir}/bjobs
%{_bindir}/bkill
%{_bindir}/bsub
%{_bindir}/lsid

#############################################################################

%files contribs
%defattr(-,root,root)
%{_bindir}/seff
%{_bindir}/sjobexitmod
%{_bindir}/sjstat
%{_bindir}/smail
%{_mandir}/man1/sjstat*
#############################################################################

%if %{with pam}
%files -f pam.files pam_slurm
%defattr(-,root,root)
%endif
#############################################################################

%if %{with slurmrestd}
%files slurmrestd
%{_sbindir}/slurmrestd
%{_unitdir}/slurmrestd.service
%endif
#############################################################################

%if %{with slurmsmwd}
%files slurmsmwd
%{_sbindir}/slurmsmwd
%{_unitdir}/slurmsmwd.service
%endif
#############################################################################

%pre

%post
/sbin/ldconfig

%preun

%postun
/sbin/ldconfig

%post slurmctld
%systemd_post slurmctld.service
%preun slurmctld
%systemd_preun slurmctld.service
%postun slurmctld
%systemd_postun_with_restart slurmctld.service

%post slurmd
%systemd_post slurmd.service
%preun slurmd
%systemd_preun slurmd.service
%postun slurmd
%systemd_postun_with_restart slurmd.service

%post slurmdbd
%systemd_post slurmdbd.service
%preun slurmdbd
%systemd_preun slurmdbd.service
%postun slurmdbd
%systemd_postun_with_restart slurmdbd.service

%changelog
* Fri Jun 21 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-57
- add restapi for scontrol reconfigure

* Thu Jun 20 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-56
- add slurmrestd.log

* Wed Jun 19 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-55
- fix sacctmgr delete error with flags specified

* Tue Jun 18 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-54
- fix the archive error

* Mon Jun 17 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-53
- enable col_data empty check

* Fri Jun 14 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-52
- fix the preempt error

* Thu Jun 13 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-51
- fix sacct not showing job information

* Wed Jun 12 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-50
- rename _get_result_by_index

* Tue Jun 11 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-49
- Add empty check and inline in set_autocommit_off

* Fri Jun 07 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-48
- Remove comments in concatenated strings

* Thu Jun 06 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-47
- replace goto fini

* Wed Jun 05 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-46
- Remove redundant tres_table fields

* Tue Jun 04 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-45
- Change the serial type to integer

* Tue Jun 04 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-44
- Fix compilation warnings

* Mon Jun 03 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-43
- Remove redundant SQL statements in adding tres

* Mon Jun 03 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-42
- fix association,GrpTRES,get_parent_limits errors

* Fri May 31 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-41
- fix querying the user binding qos

* Thu May 30 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-40
- fix adding tres resource failure

* Thu May 30 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-39
- fix the DefaultWCKey not working

* Thu May 30 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-38
- fix the sql error in adding resource

* Wed May 29 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-37
- Replace the REPLACE function with the REGEXP_REPLACE function

* Wed May 29 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-36
- fix pgsql_db_insert_ret_id

* Tue May 28 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-35
- Correcting archive SQL statements in tables

* Tue May 28 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-34
- fix some error,add pgsql_escape_str function

* Mon May 27 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-33
- Modify the alias after EXCLUDED in SQL to field name

* Mon May 27 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-32
- add partition default value in job_table

* Fri May 24 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-31
- modify the field type of info in txn_table

* Fri May 24 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-30
- Adjusting procedures

* Thu May 23 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-29
- Remove excess double quotes in assoc table

* Thu May 23 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-28
- Adjusting the commit off setting position

* Wed May 22 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-27
- update the implementation of obtaining the last result set

* Wed May 22 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-26
- modify step table

* Tue May 21 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-25
- add SQLRowCount exception handling

* Tue May 21 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-24
- fix 'user' and 'partition' field in sql

* Mon May 20 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-23
- replace UNIX_TIMESTAMP in sql

* Mon May 20 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-22
- Adjusting some SQL statements

* Fri May 17 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-21
- update 'get_parent_limits' stored procedure

* Fri May 17 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-20
- update jobcomp_table field

* Thu May 16 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-19
- resume 'autocommit off' option

* Thu May 16 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-18
- fix 'partition' conflict in tables

* Wed May 15 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-17
- remove 'FOR UPDATE' in sql

* Wed May 15 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-16
- fixed the 'partition' field in sql

* Tue May 14 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-15
- fix some sql statements

* Mon May 13 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-14
- Adjusting the implementation of last_affect_row

* Mon May 13 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-13
- use pgsql_handle_quotes to replace old method

* Sat May 11 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-12
- fix the default value type

* Fri May 10 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-11
- change sql datatype

* Tue May 07 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-10
- replace && and || in sql in some file

* Mon May 06 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-9
- add return value SQL_NO_DATA in sql api

* Tue Apr 30 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-8
- replace && and || in sql in as_pgsql_jobacct_process.c

* Mon Apr 29 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-7
- fix some sql bug

* Sun Apr 28 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-6
- add Patch3 in slurm.spec
- replace ‘&&’ with AND in sql in as_pgsql_resource.c

* Thu Apr 25 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-5
- move autoreconf command from %prep to %build

* Fri Apr 19 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-4
- add pgsql plugin.
- add BuildRequires: gtk2-devel glib2-devel for autoreconf 
- add BuildRequires: libtool for autoreconf 

* Fri Mar 15 2024 Xing Liu <liu.xingb@h3c.com> - 21.08.8-3
- support '--oversubscribe' attribute in submit_job restapi.

* Sat May 07 2022 Bruce Ma <majian@kylinos.cn> - 21.08.8-2
- Init package slurm-2.1.08.8-2 .

